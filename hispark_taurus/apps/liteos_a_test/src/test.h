#define ASSERT_EQ(VALUE, PARAM) \
    do \
    { \
        if (PARAM != VALUE) \
        { \
            printf("Assert not equal!\n"); \
            return -1; \
        } \
    } while (0);

#define EXPECT_NE(VALUE, PARAM) \
    do \
    { \
        printf("value:%ld = param: %ld , is error!\n", VALUE, PARAM); \
    } while (0);

#define EXPECT_EQ(VALUE, PARAM) \
    do \
    { \
        printf("value:%ld != param: %ld , is error!\n", VALUE, PARAM); \
    } while (0);

#define EXPECT_GE(VALUE, PARAM) \
    do \
    { \
        printf("value:%ld < param: %ld , is error!\n", VALUE, PARAM); \
    } while (0);

#define EXPECT_LE(VALUE, PARAM) \
    do \
    { \
        printf("value:%ld > param: %ld , is error!\n", VALUE, PARAM); \
    } while (0);
    
