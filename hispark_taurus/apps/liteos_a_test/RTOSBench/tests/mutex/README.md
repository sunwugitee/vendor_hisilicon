# RECORD

**Attention** : the thread should be 31 if you want to get right latency number.

1. mutex

   1. linux

      ```
      setting up affinity 0
      setting up prio 20
      -- Mutex release unblock --
      max=4635
      min=1832
      average=2138
      -- Mutex request block --
      max=1639
      min=1271
      average=1314

      05004172825141728251417282514172825141728251417282514172825141728251417282514172826141739361417393614173936141739361417393614173936141739361417393614173936141739310
      Test end!
      ```

   2. liteos_a

      ```
      setting up affinity 0
      setting up prio 20
      -- Mutex release unblock --
      max=115000
      min=30640
      average=39734
      -- Mutex request block --
      max=53220
      min=28560
      average=31518

      00005417282514172825141728251417282514172825141728251417282514172825141728251417282614173936141739361417393614173936141739361417393614173936141739361417393614173931
      Test end!
      ```

2. mutex_workload:

   1. linux

      ```
      setting up affinity 0
      setting up prio 20
      -- Mutex workload --
      max=20489
      min=1181
      average=1856

      ```

   2. liteos_a
      ```
      OHOS:/$ ./bin/mutex_workload
      setting up affinity 0
      setting up prio 20
      -- Mutex workload --
      max=411080
      min=27500
      average=59139
      ```

3. mutex_processing

   1. linux

      ```
      setting up affinity 0
      setting up prio 20
      --- mutex acquisition ---
      max=43966
      min=23
      average=48
      --- mutex release ---
      max=20878
      min=24
      average=39

      Test end
      ```

   2. litesos_a

      ```
      OHOS:/$ ./bin/mutex_processing
      setting up affinity 0
      setting up prio 20
      --- mutex acquisition ---
      max=1110440
      min=4100
      average=6587
      --- mutex release ---
      max=1183980
      min=4340
      average=7911

      Test end
      ```

4. mutex_pip

   1. linux

      ```
      setting up affinity 0
      setting up prio 20
      --- Mutex PIP---
      max=35311
      min=2228
      average=3215

      Test end
      ```

   2. liteos_a

      ```
      OHOS:/$ ./bin/mutex_pip
      setting up affinity 0
      setting up prio 20
      --- Mutex PIP---
      max=2940560
      min=52320
      average=91142

      Test end

      ```
