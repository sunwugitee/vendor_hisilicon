/* Copyright (c) 2019, Guillaume Champagne
 * All rights reserved.
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree.
 */

#include "porting_layer.h"

#define NB_ITER 7000
#define NB_TASK 2
#define NB_WORKLOAD_TASK 50
#define DELAY_MS 10
#define MQ_LEN 1
#define MQ_MSGSIZE 50

#define MQ_NAME "/12345"
#define MQ_MSGSEND "abced"

no_task_retval_t mq_initialize_test(no_task_argument_t args);
no_task_retval_t sender(no_task_argument_t args);
no_task_retval_t receiver(no_task_argument_t args);
no_task_retval_t workload_task(no_task_argument_t args);

no_task_handle_t tasks_handle[NB_TASK];
no_task_handle_t workload_tasks_handle[NB_WORKLOAD_TASK];
char workload_tasks_name[NB_WORKLOAD_TASK][4];

no_mq_t mq;

DECLARE_TIME_COUNTERS(no_time_t, _)

no_main_retval_t main(no_main_argument_t args)
{
	no_initialize_test(mq_initialize_test);
	return MAIN_DEFAULT_RETURN;
}

no_task_retval_t mq_initialize_test(no_task_argument_t args)
{
	int32_t i;

	/* Create resources */
	no_task_retval_t (*thread_array[NB_TASK])(no_task_argument_t) = {receiver, sender};

	no_mq_destory(MQ_NAME);
	no_mq_create(&mq, MQ_NAME, MQ_LEN, MQ_MSGSIZE);

	for (i = 0; i < NB_TASK; i++)
	{
		tasks_handle[i] = no_create_task(thread_array[i], NULL, BASE_PRIO - 1 + i);
	}

	for (i < 0; i < NB_WORKLOAD_TASK; i++)
	{
		workload_tasks_name[i][0] = 65;
		workload_tasks_name[i][1] = (65 + i) % 255;
		workload_tasks_name[i][2] = 0;
		workload_tasks_name[i][3] = 0;
		/* Workoad tasks are the lowest priority */
		workload_tasks_handle[i] = no_create_task(workload_task, workload_tasks_name[i], LITE_PRIO);
	}

	for (i = 0; i < NB_TASK; i++)
	{
		no_join_task(tasks_handle[i]);
	}

	no_serial_write("\nTest end!\n");
	no_mq_destory(MQ_NAME);

	return TASK_DEFAULT_RETURN;
}

no_task_retval_t sender(no_task_argument_t args)
{
	int32_t i;

	/* 2b - Benchmark. */
	for (i = 0; i < NB_ITER + 1; i++)
	{
		WRITE_T1_COUNTER(_)
		no_mq_send(mq, MQ_MSGSEND);
		no_task_delay(DELAY_MS);
	}

	no_task_suspend_self();

	return TASK_DEFAULT_RETURN;
}

no_task_retval_t receiver(no_task_argument_t args)
{
	int32_t i;
	DECLARE_TIME_STATS(int64_t)

	/* 2a - Benchmark. */
	for (i = 0; i < NB_ITER; i++)
	{
		no_mq_receive(mq);
		WRITE_T2_COUNTER(_)
		COMPUTE_TIME_STATS(_, i);
	}

	REPORT_BENCHMARK_RESULTS("-- MQ workload --")

	no_task_suspend_self();

	return TASK_DEFAULT_RETURN;
}

no_task_retval_t workload_task(no_task_argument_t args)
{
	int32_t i;
	unsigned long _workload_results[100];

	while (1)
	{
		DO_WORKLOAD(i % 100)
		no_task_yield();
		i++;
	}

	no_task_suspend_self();

	return TASK_DEFAULT_RETURN;
}
