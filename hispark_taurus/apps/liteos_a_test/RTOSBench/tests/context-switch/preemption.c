/* Copyright (c) 2019, Guillaume Champagne
 * All rights reserved.
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree.
 */

#include "porting_layer.h"
#include <math.h>

#define NB_ITER 1000
#define NB_TASK 5

no_task_retval_t task(no_task_argument_t args);
no_task_retval_t task_high(no_task_argument_t args);
no_task_retval_t preempte_initialize_test(no_task_argument_t args);

no_task_handle_t tasks_handle[NB_TASK];

DECLARE_TIME_COUNTERS(no_time_t, _)


no_main_retval_t main(no_main_argument_t args)
{
    no_initialize_test(preempte_initialize_test);
    return MAIN_DEFAULT_RETURN;
}

no_task_retval_t preempte_initialize_test(no_task_argument_t args)
{
    int32_t i;

    for ( i = 0; i < NB_TASK - 1; i++)
    {
        tasks_handle[i] = no_create_task(task, NULL, LITE_PRIO);
    }
    printf("0");

    sleep(1);

    WRITE_T1_COUNTER(_);
    tasks_handle[NB_TASK - 1] = no_create_task(task_high, NULL, BASE_PRIO);
    
    printf("1");
    long time = no_time_diff(&__t1, &__t2);
    printf("Time difference: %ld\n", time);

    for (i = 0; i < NB_TASK; i++)
	{
		no_join_task(tasks_handle[i]);
	}

    no_serial_write("\nTest End!\n");

    
}

no_task_retval_t task(no_task_argument_t args)
{
    printf("2");
    int32_t i;
    unsigned long _workload_results[100];
    while (1)
    {
        DO_WORKLOAD(i % 100);
        i++;
    }
    
}

no_task_retval_t task_high(no_task_argument_t args)
{
    printf("3");
    WRITE_T2_COUNTER(_);
}

