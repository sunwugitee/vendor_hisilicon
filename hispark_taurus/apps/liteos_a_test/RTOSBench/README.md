# RTOSBenchmark - Liteos_a

This project transplanted from the [RTOSBench](https://github.com/gchamp20/RTOSBench) is to measure the perfoemance of liteos_a at User mode by use posix interface. Also you can use this project to test posix interface availability of liteos_a.

**Attention**:

+ The project mainly tests the performance of the operating system through six aspects, including **context switching**, **signal handing**, **message queue test**, **mutex lock test**, **ticks jitter measure** and **semaphore test**. Some tests are multifaced to make sure that we can have an accurate evaluation of the performence.

+ Because the original project written at linux environment by using posix interface can get the right number on linux, I test the programm while transplanting program, and record the result not only number on my linux computer but also on liteos_A kernel on Qemu in the README.md in every test aspect dir. It must be noted that these data can only be used for reference. **The comparsion of the two data can not draw an effctive conclusion because the test environment is very different.**

+ All the effctive number get by the program are created in one process which uses [pthread_create](https://pubs.opengroup.org/onlinepubs/9699919799/functions/pthread_create.html#) to create many threads whilch are used to measure performence betweem mutli-thread. All the thread is bounds to **CPU CORE 0**. Make sure that SMP cant effect the result of measuring.

+ **Thread which was created must be suspended until all child threads creation has been finish**. It makes sure that the child thread can be schelded in fair.The priority setting can make influence to system thread schelding. So I use priority to ensure it. My main thread always has higher priority than the child thread which it creates to measure the performance. Also you can use mutex lock, just it will take an extra cost.

**Question**:

+ **Different priority settings of threads make scheduing of thread unpredictable, especially on liteos_a kernel**. In context switch test, just setting priority **31** of child threads can get the scheld sequence you want to see, nothing else(**Notes**: 31 is the smallest priority in liteos). This strange phenomenon also appears in linux. Maybe the priority which is smaller than 20 can get the right sequence(**Notes**: I dont spend much time on testing linxu kernel, mayber my found is a bug, and in linux, the bigger of priority gets higher priority). So if you want to add some examples to test liteos_a kernel, make sure that **your main thread priority number must smaller than child thread**, and the child thread prority set **31** if you can.

**TODO**:

+ Now Im very interesting in the performance behaviour on process layer of liteos_a, if I can, i will add some example to test it.
